import Analyzer
import Extractor


class Soccer():
    def findResult():
        csv_data = 'football.csv'
        data = Extractor.DataExtractor.read_data(csv_data)
        res = Analyzer.DataAnalyzer.mindiff(data, 'F', 'A', 'Team')
        return res


res = Soccer.findResult()
print(f"the name of the team {res} with the smallest difference in goals.")
