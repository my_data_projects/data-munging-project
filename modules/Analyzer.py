class DataAnalyzer:
    def mindiff(data, max_val, min_val, res):
        result = ""
        count = float(data[0][max_val]) - float(data[0][min_val])

        for eachItem in data:
            difference = abs(float(eachItem[max_val])
                             - float(eachItem[min_val]))
            if difference <= count:
                count = difference
                result = eachItem[res]
                # print(count, result)
        return(result)
