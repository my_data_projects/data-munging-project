import Analyzer
import Extractor


class Weather():
    def findResult():
        csv_data = 'weather.csv'
        data = Extractor.DataExtractor.read_data(csv_data)
        res = Analyzer.DataAnalyzer.mindiff(data, 'Max1', 'Min1', 'Day')
        return res


res = Weather.findResult()
print(f'the day number {res} with the smallest temperature spread')
