import csv


class DataExtractor:
    def read_data(filename):
        with open(filename, "r") as file:
            csvReader = csv.DictReader(file)
            data = list(csvReader)
        return data
